@extends('layouts.master')
@section('title', 'Register')
@section('content')
<section class="">
    <div class="container-fluid">
        <div class="row text-center">
            <div class="row">
                <div class="col-4 ">
            <x-guest-layout>
                
                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="" :errors="$errors" />
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="auth-box card">
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-12">
                                        <h3 class="text-center heading">Register</h3>
                                    </div>
                                </div>

                                <!-- Name -->
                                <div>
                                    <div class="block mt-1 w-full">
                                        <input id="name" class="block mt-1 w-full" type="text" name="name"
                                            :value="old('name')" required placeholder="Name" />
                                    </div>
                                </div>

                                <!-- Email Address -->


                                <div class="">
                                    <x-input id="email" class="block mt-1 w-full" type="email" name="email"
                                        :value="old('email')" required placeholder="Email" />
                                </div>

                                <!-- Password -->

                                <div class="">
                                    <x-input id="password" class="block mt-1 w-full" type="password" name="password"
                                        placeholder="Password" required autocomplete="new-password" />
                                </div>

                                <!-- Confirm Password -->
                                <div class="">
                                    <!-- <x-label for="password_confirmation" :value="__('Confirm Password')" /> -->
                                    <x-input id="password_confirmation" class="block mt-1 w-full" type="password"
                                        placeholder="password_confirmation" name="password_confirmation" required />
                                </div>
                                <div class="flex items-center justify-end mt-4">
                                    <a class="underline text-sm text-gray-600 hover:text-gray-900"
                                        href="{{ route('login') }}">
                                        {{ __('Already registered?') }}
                                    </a>
                                    <div class="text-center">
                                        <button
                                            class="btn btn-secondary btn-md btn-block waves-effect text-center m-b-20 ml-4">
                                            {{ __('Register') }}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
            </x-guest-layout>
        </div>
    </div>

</section>
@endsection